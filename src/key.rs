use core::marker::PhantomData;

/// Represents an invariant lifetime.
struct Key<'lock> {
	/// Makes the lifetime invariant.
	marker: PhantomData<fn(&'lock ()) -> &'lock ()>,
}

impl<'lock> Key<'lock> {
	/// Creates a new [`Key`].
	#[must_use]
	const fn new() -> Self {
		Self {
			marker: PhantomData,
		}
	}
}

/// A key representing the uniqueness of the value of a lock.
pub struct ValueKey<'lock> {
	#[allow(dead_code)]
	inner: Key<'lock>,
}

impl<'lock> ValueKey<'lock> {
	/// Creates a new [`ValueKey`].
	///
	/// # Safety
	///
	/// You must ensure that only one [`ValueKey`] exists at a time for a lifetime.
	#[must_use]
	pub const unsafe fn new() -> Self {
		Self { inner: Key::new() }
	}
}

/// A key representing the uniqueness of the state of a lock.
pub struct StateKey<'lock> {
	#[allow(dead_code)]
	inner: Key<'lock>,
}

impl<'lock> StateKey<'lock> {
	/// Creates a new [`StateKey`].
	///
	/// # Safety
	///
	/// You must ensure that only one [`StateKey`] exists at a time for a lifetime.
	#[must_use]
	pub const unsafe fn new() -> Self {
		Self { inner: Key::new() }
	}
}

/// Creates a keypair of [`ValueKey`] and [`StateKey`].
///
/// # Safety
///
/// You must ensure that only one of either key exists at a time for the lifetime.
#[must_use]
pub const unsafe fn pair<'lock>() -> (ValueKey<'lock>, StateKey<'lock>) {
	// SAFETY: See the documentation of this function.
	(unsafe { ValueKey::new() }, unsafe { StateKey::new() })
}
