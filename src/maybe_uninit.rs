use {
	crate::{
		guard::{Close, Init, Initness, Uninit},
		key,
		unique::Unique,
	},
	core::mem::{self, MaybeUninit},
};

/// A very funny and silly wrapper around [`MaybeUninit`].
#[repr(transparent)]
pub struct MaybeUninitLock<'lock, T> {
	inner: Unique<'lock, MaybeUninit<T>>,
}

impl<T> MaybeUninitLock<'_, T> {
	/// Uses an uninitialized lock in a scope.
	///
	/// # Example
	///
	/// ```
	/// # use muck::MaybeUninitLock;
	/// MaybeUninitLock::<i32>::new_uninit(|lock, uninit| {
	///     /* ... */
	/// });
	/// ```
	pub fn new_uninit<R, F>(scope: F) -> R
	where
		F: for<'lock> FnOnce(MaybeUninitLock<'lock, T>, Uninit<'lock>) -> R,
	{
		Self::new(MaybeUninit::uninit(), scope)
	}

	/// Uses an initialized lock in a scope.
	///
	/// # Example
	///
	/// ```
	/// # use muck::MaybeUninitLock;
	/// MaybeUninitLock::<i32>::new_init(23, |lock, init| {
	///     /* ... */
	/// });
	/// ```
	pub fn new_init<R, F>(value: T, scope: F) -> R
	where
		F: for<'lock> FnOnce(MaybeUninitLock<'lock, T>, Init<'lock>) -> R,
	{
		Self::new(MaybeUninit::new(value), |lock, uninit| {
			// SAFETY: The value is initialized.
			let init = unsafe { uninit.init() };

			scope(lock, init)
		})
	}

	/// Uses a lock in a scope.
	#[allow(clippy::new_ret_no_self)]
	pub fn new<R, F>(value: MaybeUninit<T>, scope: F) -> R
	where
		F: for<'lock> FnOnce(MaybeUninitLock<'lock, T>, Uninit<'lock>) -> R,
	{
		// SAFETY: No keys exist yet.
		let (value_key, state_key) = unsafe { key::pair() };

		let close = Close::new(value_key, state_key);
		let (this, uninit) = MaybeUninitLock::open(value, close);

		scope(this, uninit)
	}

	/// Uses a reference to a lock in a scope.
	pub fn new_ref<'a, R, F>(value: &'a MaybeUninit<T>, scope: F) -> R
	where
		F: for<'lock> FnOnce(&'a MaybeUninitLock<'lock, T>, Uninit<'lock>) -> R,
	{
		// SAFETY: No keys exist yet.
		let (value_key, state_key) = unsafe { key::pair() };

		let close = Close::new(value_key, state_key);
		let (this, uninit) = MaybeUninitLock::open_ref(value, close);

		scope(this, uninit)
	}

	/// Uses a mutable reference to a lock in a scope.
	pub fn new_mut<'a, R, F>(value: &'a mut MaybeUninit<T>, scope: F) -> R
	where
		F: for<'lock> FnOnce(&'a mut MaybeUninitLock<'lock, T>, Uninit<'lock>) -> R,
	{
		// SAFETY: No keys exist yet.
		let (value_key, state_key) = unsafe { key::pair() };

		let close = Close::new(value_key, state_key);
		let (this, uninit) = MaybeUninitLock::open_mut(value, close);

		scope(this, uninit)
	}
}

impl<'lock, T> MaybeUninitLock<'lock, T> {
	//                //
	// UNINIT METHODS //
	//                //

	/// Initializes a lock.
	///
	/// If the lock is already initialized, the old value will not be dropped.
	/// To replace the value without silently overwriting it, consider using [`Self::replace`].
	///
	/// # Example
	///
	/// ```
	/// # use muck::MaybeUninitLock;
	/// let x = MaybeUninitLock::new_uninit(|mut lock, uninit| {
	///     let init = lock.set(23, uninit);
	///     let (value, _) = lock.get(init);
	///     value
	/// });
	///
	/// assert_eq!(x, 23);
	/// ```
	pub fn set(&mut self, value: T, initness: impl Initness<'lock>) -> Init<'lock> {
		self.inner.get_mut().write(value);

		let state_key = initness.get();

		// SAFETY: The value was initialized.
		unsafe { Init::new(state_key) }
	}

	//              //
	// INIT METHODS //
	//              //

	/// Returns the value of an initialized lock, consuming it.
	pub fn get(self, init: Init<'lock>) -> (T, Close<'lock>) {
		let (value, value_key) = self.inner.get();
		let state_key = init.get();
		let close = Close::new(value_key, state_key);

		// SAFETY: The init guard ensures the value is initialized.
		let value = unsafe { value.assume_init() };

		(value, close)
	}

	/// Returns a reference to the value of an initialized lock.
	#[must_use]
	pub const fn get_ref(&self, #[allow(unused_variables)] init: &Init<'lock>) -> &T {
		// SAFETY: The init guard ensures the value is initialized.
		unsafe { self.inner.get_ref().assume_init_ref() }
	}

	/// Returns a mutable reference to the value of an initialized lock.
	#[must_use]
	pub fn get_mut(&mut self, #[allow(unused_variables)] init: &Init<'lock>) -> &mut T {
		// SAFETY: The init guard ensures the value is initialized.
		unsafe { self.inner.get_mut().assume_init_mut() }
	}

	/// Takes and returns the value of an initialized lock, uninitializing it.
	pub fn take(&mut self, init: Init<'lock>) -> (T, Uninit<'lock>) {
		let old = mem::replace(self.inner.get_mut(), MaybeUninit::uninit());

		// SAFETY: The init guard ensures the old value is initialized.
		let value = unsafe { old.assume_init() };

		let state_key = init.get();
		let uninit = Uninit::new(state_key);

		(value, uninit)
	}

	/// Drops the value of an initialized lock in place, uninitializing it.
	pub fn drop(&mut self, init: Init<'lock>) -> Uninit<'lock> {
		// SAFETY: The init guard ensures the value is initialized.
		unsafe {
			self.inner.get_mut().assume_init_drop();
		}

		let state_key = init.get();
		Uninit::new(state_key)
	}

	/// Replaces the value of an initialized lock, returning the old value.
	pub fn replace(&mut self, value: T, init: Init<'lock>) -> (T, Init<'lock>) {
		let (old, uninit) = self.take(init);
		let init = self.set(value, uninit);
		(old, init)
	}

	//               //
	// CLOSE METHODS //
	//               //

	/// Consumes the lock.
	///
	/// The value will not be dropped.
	/// To drop an initialized value, consider using [`Self::get`], [`Self::take`], or [`Self::drop`].
	///
	/// # Example
	///
	/// ```
	/// # use muck::MaybeUninitLock;
	/// let x = MaybeUninitLock::new_uninit(|mut lock, uninit| {
	///     let init = lock.set(32, uninit);
	///     let close = lock.close(init);
	///
	///     let (mut lock, uninit) = MaybeUninitLock::open_uninit(close);
	///     let init = lock.set("number 15", uninit); // a new type!
	///     let (value, _) = lock.get(init);
	///     value
	/// });
	///
	/// assert_eq!(x, "number 15");
	/// ```
	#[must_use]
	pub fn close(self, initness: impl Initness<'lock>) -> Close<'lock> {
		let (_, value_key) = self.inner.get();
		let state_key = initness.get();
		Close::new(value_key, state_key)
	}

	/// Creates an uninitialized lock.
	#[must_use]
	pub const fn open_uninit(close: Close<'lock>) -> (Self, Uninit<'lock>) {
		Self::open(MaybeUninit::uninit(), close)
	}

	/// Creates an initialized lock.
	#[must_use]
	pub const fn open_init(value: T, close: Close<'lock>) -> (Self, Init<'lock>) {
		let (this, uninit) = Self::open(MaybeUninit::new(value), close);

		// SAFETY: The value is initialized.
		let init = unsafe { uninit.init() };

		(this, init)
	}

	/// Creates a lock by value.
	#[must_use]
	pub const fn open(value: MaybeUninit<T>, close: Close<'lock>) -> (Self, Uninit<'lock>) {
		let (value_key, state_key) = close.get();

		let this = Self {
			inner: Unique::new(value, value_key),
		};

		// We assume the value is uninitialized.
		let uninit = Uninit::new(state_key);

		(this, uninit)
	}

	/// Creates a lock by reference.
	#[must_use]
	pub const fn open_ref<'a>(
		value: &'a MaybeUninit<T>,
		close: Close<'lock>,
	) -> (&'a Self, Uninit<'lock>) {
		let (value_key, state_key) = close.get();

		// SAFETY: This type is `repr(transparent)` over `Unique<T>`.
		let this = unsafe {
			&*(Unique::new_ref(value, value_key) as *const Unique<'lock, MaybeUninit<T>>
				as *const Self)
		};

		// We assume the value is uninitialized.
		let uninit = Uninit::new(state_key);

		(this, uninit)
	}

	/// Creates a lock by mutable reference.
	#[must_use]
	pub fn open_mut<'a>(
		value: &'a mut MaybeUninit<T>,
		close: Close<'lock>,
	) -> (&'a mut Self, Uninit<'lock>) {
		let (value_key, state_key) = close.get();

		// SAFETY: This type is `repr(transparent)` over `Unique<T>`.
		let this = unsafe {
			&mut *(Unique::new_mut(value, value_key) as *mut Unique<'lock, MaybeUninit<T>>
				as *mut Self)
		};

		// We assume the value is uninitialized.
		let uninit = Uninit::new(state_key);

		(this, uninit)
	}

	//             //
	// RAW METHODS //
	//             //

	/// Returns the inner [`MaybeUninit`] value, consuming the lock.
	#[must_use]
	pub fn raw(self, initness: impl Initness<'lock>) -> (MaybeUninit<T>, Close<'lock>) {
		let (value, value_key) = self.inner.get();
		let state_key = initness.get();
		let close = Close::new(value_key, state_key);

		(value, close)
	}

	/// Returns a reference to the inner [`MaybeUninit`] value.
	#[must_use]
	pub const fn raw_ref(&self) -> &MaybeUninit<T> {
		self.inner.get_ref()
	}

	/// Returns a mutable reference to the inner [`MaybeUninit`] value.
	///
	/// # Safety
	///
	/// You must ensure the value does not become uninitialized while [`Init`] is active.
	/// If this does occur, use [`Init::uninit`] to downgrade to [`Uninit`].
	///
	/// On the other hand, if you're initializing the value while [`Uninit`] is active, use [`Uninit::init`] once the value is initialized to upgrade to [`Init`].
	#[must_use]
	pub unsafe fn raw_mut(&mut self) -> &mut MaybeUninit<T> {
		self.inner.get_mut()
	}
}

#[allow(clippy::undocumented_unsafe_blocks)]
#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn roundabout() {
		let x = MaybeUninitLock::<i32>::new_uninit(|mut lock, uninit| {
			let raw = unsafe { lock.raw_mut() };
			MaybeUninitLock::<i32>::new_mut(raw, |lock, uninit| {
				lock.set(42, uninit);
			});
			let init = unsafe { uninit.init() };

			let (value, _) = lock.get(init);
			value
		});

		assert_eq!(x, 42);
	}
}
