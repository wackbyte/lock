use crate::key::ValueKey;

/// A unique value.
///
/// Its uniqueness is based on its ownership of a [`ValueKey`].
#[repr(transparent)]
pub struct Unique<'lock, T>
where
	T: ?Sized,
{
	value_key: ValueKey<'lock>,
	value: T,
}

impl<'lock, T> Unique<'lock, T> {
	/// Creates a unique value.
	#[must_use]
	pub const fn new(value: T, value_key: ValueKey<'lock>) -> Self {
		Self { value_key, value }
	}

	/// Extracts the value and key.
	#[must_use]
	pub fn get(self) -> (T, ValueKey<'lock>) {
		(self.value, self.value_key)
	}
}

impl<'lock, T> Unique<'lock, T>
where
	T: ?Sized,
{
	/// Creates a unique reference.
	#[allow(clippy::needless_pass_by_value)]
	#[must_use]
	pub const fn new_ref<'a>(
		value: &'a T,
		#[allow(unused_variables)] value_key: ValueKey<'lock>,
	) -> &'a Self {
		// SAFETY:
		// - The key is consumed.
		// - This type is `repr(transparent)` over `T`.
		unsafe { &*(value as *const T as *const Self) }
	}

	/// Creates a unique mutable reference.
	#[allow(clippy::needless_pass_by_value)]
	#[must_use]
	pub fn new_mut<'a>(
		value: &'a mut T,
		#[allow(unused_variables)] value_key: ValueKey<'lock>,
	) -> &'a mut Self {
		// SAFETY:
		// - The key is consumed.
		// - This type is `repr(transparent)` over `T`.
		unsafe { &mut *(value as *mut T as *mut Self) }
	}

	/// Returns a reference to the value.
	#[must_use]
	pub const fn get_ref(&self) -> &T {
		&self.value
	}

	/// Returns a mutable reference to the value.
	#[must_use]
	pub fn get_mut(&mut self) -> &mut T {
		&mut self.value
	}
}
