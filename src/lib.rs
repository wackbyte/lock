//! # `muck`
//!
//! Wrappers.

// Attributes
#![cfg_attr(not(any(doc, test)), no_std)]
// Lints
#![allow(clippy::module_name_repetitions, clippy::ptr_as_ptr)]
#![warn(clippy::missing_const_for_fn, clippy::must_use_candidate, missing_docs)]
#![deny(
	clippy::multiple_unsafe_ops_per_block,
	clippy::undocumented_unsafe_blocks,
	clippy::transmute_ptr_to_ptr,
	clippy::transmute_ptr_to_ref
)]
#![forbid(unsafe_op_in_unsafe_fn)]

mod guard;
mod key;
mod unique;

mod manually_drop;
mod maybe_uninit;

pub use self::{
	guard::{Close, Init, Initness, Uninit, UninitOrInit},
	manually_drop::ManuallyDropLock,
	maybe_uninit::MaybeUninitLock,
};
