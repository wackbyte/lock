use crate::key::{StateKey, ValueKey};

/// A guard representing an uninitialized lock.
///
/// Allows initialization of the value.
pub struct Uninit<'lock> {
	state_key: StateKey<'lock>,
}

impl<'lock> Uninit<'lock> {
	/// Creates a guard.
	#[must_use]
	pub(crate) const fn new(state_key: StateKey<'lock>) -> Self {
		Self { state_key }
	}

	/// Extracts the key.
	#[must_use]
	pub(crate) const fn get(self) -> StateKey<'lock> {
		self.state_key
	}

	/// Transforms into [`Init`], assuming the lock's value as initialized.
	///
	/// # Safety
	///
	/// The lock's value must be initialized.
	#[must_use]
	pub const unsafe fn init(self) -> Init<'lock> {
		// SAFETY: See the documentation of this function.
		unsafe { Init::new(self.get()) }
	}
}

/// A guard representing an initialized lock.
///
/// Allows access to the initialized value.
pub struct Init<'lock> {
	state_key: StateKey<'lock>,
}

impl<'lock> Init<'lock> {
	/// Creates a guard.
	///
	/// # Safety
	///
	/// The lock's value must be initialized.
	#[must_use]
	pub(crate) const unsafe fn new(state_key: StateKey<'lock>) -> Self {
		Self { state_key }
	}

	/// Extracts the key.
	#[must_use]
	pub(crate) const fn get(self) -> StateKey<'lock> {
		self.state_key
	}

	/// Transforms into [`Uninit`], assuming the lock's value as uninitialized.
	#[must_use]
	pub const fn uninit(self) -> Uninit<'lock> {
		Uninit::new(self.get())
	}
}

/// Either [`Uninit`] or [`Init`].
pub enum UninitOrInit<'lock> {
	/// [`Uninit`].
	Uninit(Uninit<'lock>),
	/// [`Init`].
	Init(Init<'lock>),
}

impl<'lock> UninitOrInit<'lock> {
	/// Extracts the key.
	#[must_use]
	pub(crate) const fn get(self) -> StateKey<'lock> {
		match self {
			Self::Uninit(uninit) => uninit.get(),
			Self::Init(init) => init.get(),
		}
	}
}

impl<'lock> From<Uninit<'lock>> for UninitOrInit<'lock> {
	fn from(uninit: Uninit<'lock>) -> Self {
		Self::Uninit(uninit)
	}
}

impl<'lock> From<Init<'lock>> for UninitOrInit<'lock> {
	fn from(init: Init<'lock>) -> Self {
		Self::Init(init)
	}
}

/// A guard representing a consumed lock.
///
/// Allows a new lock to be created in place of the consumed one.
pub struct Close<'lock> {
	value_key: ValueKey<'lock>,
	state_key: StateKey<'lock>,
}

impl<'lock> Close<'lock> {
	/// Creates a guard.
	#[must_use]
	pub(crate) const fn new(value_key: ValueKey<'lock>, state_key: StateKey<'lock>) -> Self {
		Self {
			value_key,
			state_key,
		}
	}

	/// Extracts the keys.
	#[must_use]
	pub(crate) const fn get(self) -> (ValueKey<'lock>, StateKey<'lock>) {
		(self.value_key, self.state_key)
	}
}

mod sealed {
	use super::{Init, StateKey, Uninit, UninitOrInit};

	pub trait Sealed<'lock> {
		#[must_use]
		fn get(self) -> StateKey<'lock>;
	}

	impl<'lock> Sealed<'lock> for Uninit<'lock> {
		fn get(self) -> StateKey<'lock> {
			self.get()
		}
	}

	impl<'lock> Sealed<'lock> for Init<'lock> {
		fn get(self) -> StateKey<'lock> {
			self.get()
		}
	}

	impl<'lock> Sealed<'lock> for UninitOrInit<'lock> {
		fn get(self) -> StateKey<'lock> {
			self.get()
		}
	}
}

use sealed::Sealed;

/// A guard representing the state of a lock.
///
/// The state can be either [`Uninit`] or [`Init`].
pub trait Initness<'lock>: Sealed<'lock> {}

impl<'lock> Initness<'lock> for Uninit<'lock> {}

impl<'lock> Initness<'lock> for Init<'lock> {}

impl<'lock> Initness<'lock> for UninitOrInit<'lock> {}
