use {
	crate::{
		guard::{Close, Init, Initness, Uninit},
		key,
		unique::Unique,
	},
	core::mem::ManuallyDrop,
};

/// A very funny and silly wrapper around [`ManuallyDrop`].
pub struct ManuallyDropLock<'lock, T>
where
	T: ?Sized,
{
	inner: Unique<'lock, ManuallyDrop<T>>,
}

impl<T> ManuallyDropLock<'_, T>
where
	T: ?Sized,
{
	/// Uses a lock in a scope.
	#[allow(clippy::new_ret_no_self)]
	pub fn new<R, F>(value: ManuallyDrop<T>, scope: F) -> R
	where
		T: Sized,
		F: for<'lock> FnOnce(ManuallyDropLock<'lock, T>, Init<'lock>) -> R,
	{
		// SAFETY: No keys exist yet.
		let (value_key, state_key) = unsafe { key::pair() };

		let close = Close::new(value_key, state_key);
		let (this, init) = ManuallyDropLock::open(value, close);

		scope(this, init)
	}

	/// Uses a reference to a lock in a scope.
	pub fn new_ref<'a, R, F>(value: &'a ManuallyDrop<T>, scope: F) -> R
	where
		F: for<'lock> FnOnce(&'a ManuallyDropLock<'lock, T>, Init<'lock>) -> R,
	{
		// SAFETY: No keys exist yet.
		let (value_key, state_key) = unsafe { key::pair() };

		let close = Close::new(value_key, state_key);
		let (this, init) = ManuallyDropLock::open_ref(value, close);

		scope(this, init)
	}

	/// Uses a mutable reference to a lock in a scope.
	///
	/// # Safety
	///
	/// The value must not be used if it is dropped.
	pub unsafe fn new_mut<'a, R, F>(value: &'a mut ManuallyDrop<T>, scope: F) -> R
	where
		F: for<'lock> FnOnce(&'a mut ManuallyDropLock<'lock, T>, Init<'lock>) -> R,
	{
		// IDEA: Require the scope to return an `Init<'lock>` in order to ensure that the value was not dropped?

		// SAFETY: No keys exist yet.
		let (value_key, state_key) = unsafe { key::pair() };

		let close = Close::new(value_key, state_key);

		// SAFETY: See the documentation of this function.
		let (this, init) = unsafe { ManuallyDropLock::open_mut(value, close) };

		scope(this, init)
	}
}

impl<'lock, T> ManuallyDropLock<'lock, T>
where
	T: ?Sized,
{
	//                //
	// UNINIT METHODS //
	//                //

	/// Sets the value of a lock, ensuring that it is not dropped.
	///
	/// If the value is already set, it will not be dropped.
	/// To replace the value without silently overwriting it, consider using [`Self::replace`].
	pub fn set(&mut self, value: T, initness: impl Initness<'lock>) -> Init<'lock>
	where
		T: Sized,
	{
		*self.inner.get_mut() = ManuallyDrop::new(value);

		let state_key = initness.get();

		// SAFETY: The possibly-dropped value was re-initialized.
		unsafe { Init::new(state_key) }
	}

	//              //
	// INIT METHODS //
	//              //

	/// Returns the value of an undropped lock, consuming it.
	pub fn get(self, init: Init<'lock>) -> (T, Close<'lock>)
	where
		T: Sized,
	{
		let (value, value_key) = self.inner.get();
		let state_key = init.get();
		let close = Close::new(value_key, state_key);

		let value = ManuallyDrop::into_inner(value);

		(value, close)
	}

	/// Returns a reference to the value of an undropped lock.
	#[must_use]
	pub fn get_ref(&self, #[allow(unused_variables)] init: &Init<'lock>) -> &T {
		// SAFETY: The init guard ensures the value has not been dropped.
		self.inner.get_ref()
	}

	/// Returns a mutable reference to the value of an undropped lock.
	#[must_use]
	pub fn get_mut(&mut self, #[allow(unused_variables)] init: &Init<'lock>) -> &mut T {
		// SAFETY: The init guard ensures the value has not been dropped.
		self.inner.get_mut()
	}

	/// Takes and returns the value of an undropped lock, copying it but ensuring the old value cannot be used again.
	pub fn take(&mut self, init: Init<'lock>) -> (T, Uninit<'lock>)
	where
		T: Sized,
	{
		// SAFETY: The init guard is consumed, ensuring this function cannot be called more than once.
		let value = unsafe { ManuallyDrop::take(self.inner.get_mut()) };

		let state_key = init.get();
		let uninit_guard = Uninit::new(state_key);

		(value, uninit_guard)
	}

	/// Drops the value of an undropped lock, ensuring the old value cannot be used again.
	pub fn drop(&mut self, init: Init<'lock>) -> Uninit<'lock> {
		// SAFETY: The init guard is consumed, ensuring this function cannot be called more than once.
		unsafe {
			ManuallyDrop::drop(self.inner.get_mut());
		}

		let state_key = init.get();
		Uninit::new(state_key)
	}

	/// Replaces the value of an undropped lock.
	pub fn replace(&mut self, value: T, init: Init<'lock>) -> (T, Init<'lock>)
	where
		T: Sized,
	{
		let (old, uninit) = self.take(init);
		let init = self.set(value, uninit);
		(old, init)
	}

	//               //
	// CLOSE METHODS //
	//               //

	/// Consumes a lock.
	///
	/// The value will not be dropped.
	/// To drop an initialized value, consider using [`Self::get`], [`Self::take`], or [`Self::drop`].
	#[must_use]
	pub fn close(self, initness: impl Initness<'lock>) -> Close<'lock>
	where
		T: Sized,
	{
		let (_, value_key) = self.inner.get();
		let state_key = initness.get();
		Close::new(value_key, state_key)
	}

	/// Creates a lock by value.
	#[must_use]
	pub const fn open(value: ManuallyDrop<T>, close: Close<'lock>) -> (Self, Init<'lock>)
	where
		T: Sized,
	{
		let (value_key, state_key) = close.get();

		let this = Self {
			inner: Unique::new(value, value_key),
		};

		// SAFETY: We assume the value has not been dropped.
		let init = unsafe { Init::new(state_key) };

		(this, init)
	}

	/// Creates a lock by reference.
	#[must_use]
	pub const fn open_ref<'a>(
		value: &'a ManuallyDrop<T>,
		close: Close<'lock>,
	) -> (&'a Self, Init<'lock>) {
		let (value_key, state_key) = close.get();

		// SAFETY: This type is `repr(transparent)` over `Unique<T>`.
		let this = unsafe {
			&*(Unique::new_ref(value, value_key) as *const Unique<'lock, ManuallyDrop<T>>
				as *const Self)
		};

		// SAFETY: We assume the value has not been dropped.
		let init = unsafe { Init::new(state_key) };

		(this, init)
	}

	/// Creates a lock by mutable reference.
	///
	/// # Safety
	///
	/// The value must not be used if it is dropped.
	#[must_use]
	pub unsafe fn open_mut<'a>(
		value: &'a mut ManuallyDrop<T>,
		close: Close<'lock>,
	) -> (&'a mut Self, Init<'lock>) {
		let (value_key, state_key) = close.get();

		// SAFETY: This type is `repr(transparent)` over `Unique<T>`.
		let this = unsafe {
			&mut *(Unique::new_mut(value, value_key) as *mut Unique<'lock, ManuallyDrop<T>>
				as *mut Self)
		};

		// SAFETY: We assume the value has not been dropped.
		let init = unsafe { Init::new(state_key) };

		(this, init)
	}

	//             //
	// RAW METHODS //
	//             //

	/// Returns the inner [`ManuallyDrop`] value, consuming the lock.
	///
	/// # Safety
	///
	/// The value may have been dropped.
	#[must_use]
	pub unsafe fn raw(self, initness: impl Initness<'lock>) -> (ManuallyDrop<T>, Close<'lock>)
	where
		T: Sized,
	{
		let (value, value_key) = self.inner.get();
		let state_key = initness.get();
		let close = Close::new(value_key, state_key);

		(value, close)
	}

	/// Returns a reference to the inner [`ManuallyDrop`] value.
	///
	/// # Safety
	///
	/// The value may have been dropped.
	#[must_use]
	pub const unsafe fn raw_ref(&self) -> &ManuallyDrop<T> {
		self.inner.get_ref()
	}

	/// Returns a mutable reference to the inner [`ManuallyDrop`] value.
	///
	/// # Safety
	///
	/// The value may have been dropped.
	#[must_use]
	pub unsafe fn raw_mut(&mut self) -> &mut ManuallyDrop<T> {
		self.inner.get_mut()
	}
}
